import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './app-sidebar.component.html',
  styleUrls: ['./app-sidebar.component.scss'],
})
export class AppSidebarComponent implements OnInit {
  @Input() event: Event;
  constructor() {
    setTimeout(() => {
      console.log(event);
    }, 4000);
  }

  ngOnInit(): void {}
}
