import { Component, OnInit, Inject, NgZone, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  private chart: am4charts.XYChart;
  data: any;
  chartData: any[];
  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private zone: NgZone,
    private api: DataService
  ) {}

  ngOnInit(): void {
    this.getData();
  }
  getData() {
    this.api.getDashboardData().subscribe(
      (data) => {
        console.log(data);
        this.data = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }
  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {
      am4core.useTheme(am4themes_animated);

      am4core.useTheme(am4themes_animated);
      // Themes end

      // Create chart instance

      // Add data
      var chart = am4core.create('chartdiv', am4charts.XYChart);
      setTimeout(() => {
        chart.paddingRight = 20;
        chart.data = this.data.analytics;
      }, 1000);

      // Create axes
      let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = 'date';
      categoryAxis.renderer.minGridDistance = 50;
      categoryAxis.renderer.grid.template.location = 0.5;
      categoryAxis.startLocation = 0.5;
      categoryAxis.endLocation = 0.5;
      categoryAxis.renderer.grid.template.disabled = true;

      // Create value axis
      let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.baseValue = 0;

      // Create series
      let series = chart.series.push(new am4charts.LineSeries());
      series.dataFields.valueY = 'balance';
      series.dataFields.categoryX = 'date';
      series.strokeWidth = 2.5;
      series.cursorOptions.overStyle;
      series.tensionX = 0.77;

      // bullet is added because we add tooltip to a bullet for it to change color
      let bullet = series.bullets.push(new am4charts.Bullet());
      bullet.tooltipText = '{valueY}';

      chart.cursor = new am4charts.XYCursor();

      this.chart = chart;
    });
  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
}
