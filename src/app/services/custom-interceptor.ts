import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable , throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { tap, catchError } from 'rxjs/operators'




@Injectable({
    providedIn: 'root'
  })
export class CustomInterceptor implements HttpInterceptor {
    constructor(public router: Router) { }
    
    getToken() {
        return localStorage.getItem("token")  || "";
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({
            url: environment.BASE_URL + request.url,
            setHeaders: {
                'Authorization': `Bearer ${this.getToken()}`,
                'Content-Type': 'application/json'
            }
        });
        return next.handle(request)
            .pipe(tap((event: HttpEvent<any>) => {
            }, (err: any) => {
                // this.util.loading.next(false);
                if (err instanceof HttpErrorResponse) {
                    console.log(err.status);
                    if (err.status === 401 || err.status === 0) {
                        localStorage.removeItem("token");
                        localStorage.removeItem("user");
                        this.router.navigate(['/login']);
                    }
                }
            }))
        // .catch((error,caught)=>{
        //     return Observable.throw(error.error);
        // });
    }
}